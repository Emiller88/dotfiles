# Run locally installed bin-script, e.g. n coffee file.coffee
alias n='PATH="$(npm bin):$PATH"'

# yarn
alias yart='yarn start'
