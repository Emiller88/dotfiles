alias py=python
alias py2=python2
alias py3=python3

alias pe=pipenv
alias po=poetry
alias ipy=ipython
alias ipylab='ipython --pylab=qt5 --no-banner'
