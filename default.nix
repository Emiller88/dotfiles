{ config, pkgs, options, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    <home-manager/nixos>
    /etc/nixos/hardware-configuration.nix
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      grub = {
        useOSProber = true;
        configurationLimit = 30;
      };
      efi.canTouchEfiVariables = true;
    };
    cleanTmpDir = true;
    plymouth.enable = true;
  };

  nixpkgs.config.allowUnfree = true;

  # nixpkgs.overlays = [
  #   (import ./overlays/lorri.nix)
  # ];

  environment = {
    systemPackages = with pkgs; [
      coreutils
      git
      wget
      vim
      gnupg
      unzip
      bc
      (ripgrep.override { withPCRE2 = true; })
      bashmount
    ];
    variables = {
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_CACHE_HOME = "$HOME/.cache";
      XDG_DATA_HOME = "$HOME/.local/share";
      XDG_BIN_HOME = "$HOME/.local/bin";
      DOTFILES = "$HOME/.dotfiles";
      # GTK2_RC_FILES = "$HOME/.config/gtk-2.0/gtkrc";
    };
    shellAliases = {
      q = "exit";
      clr = "clear";
      sudo = "sudo ";
      nix-env = "NIXPKGS_ALLOW_UNFREE=1 nix-env";
      ne = "nix-env";
      nu =
      "sudo nix-channel --update && sudo nixos-rebuild -I config=$HOME/.dotfiles/config switch";
      nre = "sudo nixos-rebuild -I config=$HOME/.dotfiles/config";
      ngc = "nix-collect-garbage -d && sudo nix-collect-garbage -d";
      ns = "nix-shell";
    };
  };

  time.timeZone = "America/Chicago";

  # Block well known bad hosts
  networking.extraHosts = builtins.readFile (builtins.fetchurl {
    name = "blocked_hosts.txt";
    url =
    "http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext";
  });

  nix.trustedUsers = [ "root" "@wheel" ];
  nix.nixPath = options.nix.nixPath.default ++ [ "config=${./config}" ];
  users.users.emiller = {
    home = "/home/emiller";
    isNormalUser = true;
    uid = 1000;
    description = "Edmund Miller";
    name = "emiller";
    extraGroups = [ "wheel" "video" ];
    shell = pkgs.zsh;
    openssh = { authorizedKeys.keys = [ "/home/emiller/.ssh/id_rsa" ]; };
  };

  home-manager.users.emiller = {
    xdg.enable = true;
    home.file."bin" = {
      source = ./bin;
      recursive = true;
    };
  };

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?
}
